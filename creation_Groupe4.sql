/*
Création des relations 
*/

prompt "Création des relations"

CREATE TABLE PERSONNE (
	ID_PERSONNE NUMERIC(6,0),
	NOM VARCHAR(15),
	PRENOM VARCHAR(15),
	CONSTRAINT PK_ID_PERSONNE PRIMARY KEY (ID_PERSONNE)
);

CREATE TABLE CLIENT (
	ID_CLIENT NUMERIC(6,0),
	MAIL VARCHAR(50),
	CONSTRAINT PK_ID_CLIENT PRIMARY KEY (ID_CLIENT),
	CONSTRAINT FK_ID_CLIENT FOREIGN KEY (ID_CLIENT) REFERENCES PERSONNE(ID_PERSONNE) ON DELETE CASCADE
);

CREATE TABLE RESTAURANT (
	ID_RESTO NUMERIC(6,0),
	NOM VARCHAR(30),
	ADRESS VARCHAR(50),
	CONSTRAINT PK_ID_RESTO PRIMARY KEY (ID_RESTO)
);

CREATE TABLE EMPLOYE (
	ID_EMPLOYE NUMERIC(6,0),
	ID_RESTO NUMERIC(6,0),
	TYPE VARCHAR(15) CHECK (TYPE IN ('PATRON','SERVEUR','CUISINIER')),
	CONSTRAINT PK_ID_EMPLOYE PRIMARY KEY (ID_EMPLOYE),
	CONSTRAINT FK_ID_EMPLOYE_RESTO FOREIGN KEY (ID_RESTO) REFERENCES RESTAURANT(ID_RESTO) ON DELETE CASCADE,
	CONSTRAINT FK_ID_EMPLOYE FOREIGN KEY (ID_EMPLOYE) REFERENCES PERSONNE(ID_PERSONNE) ON DELETE CASCADE
);

CREATE TABLE TABLES (
	ID_TABLE NUMERIC(6,0),
	ID_RESTO NUMERIC(6,0),
	NB_PLACES NUMERIC(6,0),
	CONSTRAINT PK_TABLE_RESTO PRIMARY KEY (ID_TABLE),
	CONSTRAINT FK_ID_TABLE_RESTO FOREIGN KEY (ID_RESTO) REFERENCES RESTAURANT(ID_RESTO) ON DELETE CASCADE
);

CREATE TABLE RESERVATION (
	ID_TABLE NUMERIC(6,0),
	ID_CLIENT NUMERIC(6,0),
	DEBUT TIMESTAMP,
	FIN TIMESTAMP,
	NB_PERSONNE NUMERIC(6,0),
	CONSTRAINT PK_RESERVATION PRIMARY KEY (ID_TABLE,ID_CLIENT),
	CONSTRAINT FK_ID_TABLE FOREIGN KEY (ID_TABLE) REFERENCES TABLES(ID_TABLE) ON DELETE CASCADE ,
	CONSTRAINT FK_RESERV_CLIENT FOREIGN KEY (ID_CLIENT) REFERENCES CLIENT(ID_CLIENT) ON DELETE CASCADE
);

CREATE TABLE AVIS (
	ID_RESTO NUMERIC(6,0),
	ID_CLIENT NUMERIC(6,0),
	NOTE INT CHECK (NOTE BETWEEN 0 and 5),
	COMMENTAIRE VARCHAR(50),
	CONSTRAINT PK_AVIS PRIMARY KEY (ID_RESTO,ID_CLIENT),
	CONSTRAINT FK_AVIS_RESTO FOREIGN KEY (ID_RESTO) REFERENCES RESTAURANT(ID_RESTO) ON DELETE CASCADE,
	CONSTRAINT FK_AVIS_CLIENT FOREIGN KEY (ID_CLIENT) REFERENCES CLIENT(ID_CLIENT) ON DELETE CASCADE
);

CREATE TABLE INTOLERANCE (
	ID_INTO VARCHAR(20),
	DESCRIPTION VARCHAR(50),
	CONSTRAINT PK_ID_INTO PRIMARY KEY (ID_INTO)
);

CREATE TABLE INTOLERANT (
	ID_CLIENT NUMERIC(6,0),
	ID_INTO VARCHAR(15),
	CONSTRAINT PK_INTO_CLIENT PRIMARY KEY (ID_CLIENT,ID_INTO),
	CONSTRAINT FK_INTO_CLIENT FOREIGN KEY (ID_CLIENT) REFERENCES CLIENT(ID_CLIENT) ON DELETE CASCADE,
	CONSTRAINT FK_ID_INTO FOREIGN KEY (ID_INTO) REFERENCES INTOLERANCE(ID_INTO) ON DELETE CASCADE 
);




------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------


prompt --- PROCEDURES ---;

CREATE OR REPLACE PROCEDURE afficherReserv (nom_resto IN varchar) 
IS
varia number;
varia2 varchar(20);
id_rest number;
BEGIN
	SELECT ID_RESTO INTO id_rest FROM RESTAURANT WHERE NOM=nom_resto;
	
	FOR reserv IN (SELECT * FROM RESERVATION)
	LOOP
		SELECT TABLES.ID_RESTO INTO varia FROM TABLES WHERE TABLES.ID_TABLE=reserv.ID_TABLE;
		IF varia=id_rest THEN
			SELECT NOM INTO varia2 FROM PERSONNE WHERE ID_PERSONNE=reserv.ID_CLIENT;
			dbms_output.put_line('>> ' || varia2);
		END IF;
	END LOOP;
END;
/

--- FONCTIONS ---

CREATE OR REPLACE FUNCTION clientDansResto(id_client in number, nom_resto in varchar) return boolean
IS
varia number;
id_rest number;
BEGIN
	SELECT ID_RESTO INTO id_rest FROM RESTAURANT WHERE NOM=nom_resto;
	
	FOR reserv IN (SELECT * FROM RESERVATION)
	LOOP
		SELECT TABLES.ID_RESTO INTO varia FROM TABLES WHERE TABLES.ID_TABLE=reserv.ID_TABLE;
		IF varia=id_rest AND reserv.ID_CLIENT=id_client THEN
			return TRUE;
		END IF;
	END LOOP;
	
	return FALSE;
END;
/

--- TRIGGERS ---

CREATE OR REPLACE TRIGGER reservationIntos
before
	INSERT ON RESERVATION FOR EACH ROW
BEGIN
	FOR intol in (
		SELECT INTOLERANCE.ID_INTO FROM INTOLERANCE,INTOLERANT 
		WHERE INTOLERANT.ID_CLIENT=:new.ID_CLIENT AND INTOLERANCE.ID_INTO=INTOLERANT.ID_INTO)
	LOOP
		dbms_output.put_line(intol.ID_INTO);
	END LOOP;
END;
/

CREATE OR REPLACE TRIGGER newClient
after
	INSERT ON CLIENT
BEGIN
	dbms_output.put_line('Noubliez pas de renseigner vos intolerances');
END;
/

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------



ALTER session SET NLS_DATE_FORMAT='DD-MM-YYYY' ; 

/* 
Paramètres d'affichage
*/




SET PAGESIZE 30
COLUMN COLUMN_NAME FORMAT A30
SET LINESIZE 300
/*
Effacer les anciennes valeurs des relations
*/

prompt -------------------------------------------;
prompt --- Suppression des anciens tuples --------;
prompt -------------------------------------------;
DELETE FROM PERSONNE; 
DELETE FROM CLIENT; 
DELETE FROM RESTAURANT; 
DELETE FROM EMPLOYE; 
DELETE FROM TABLES; 
DELETE FROM RESERVATION; 
DELETE FROM AVIS; 
DELETE FROM INTOLERANCE; 
DELETE FROM INTOLERANT; 

prompt ------------------------------------------;
prompt -----     insertion INTOLERANCES    ------------;
prompt ------------------------------------------;


INSERT INTO INTOLERANCE VALUES ('VEGAN',' pas de produits provenant danimaux') ;  
INSERT INTO INTOLERANCE VALUES ('VEGETARIEN',' pas de viandes') ;  
INSERT INTO INTOLERANCE VALUES ('LACTOSE',' tout ce qui est a base de lait') ;  
INSERT INTO INTOLERANCE VALUES ('GLUTEN',' tout qui contient du gluten') ;  
INSERT INTO INTOLERANCE VALUES ('ARACHIDE',' tout ce qui contient des arachides') ;  
INSERT INTO INTOLERANCE VALUES ('DIABETE',' pas trop de sucre') ;  
INSERT INTO INTOLERANCE VALUES ('TYRAMINE',' fromage , vin rouge, avocat, framboise ...') ; 
INSERT INTO INTOLERANCE VALUES ('FRUIT DE MER',' poisson, crustaces, mollusques') ;  
INSERT INTO INTOLERANCE VALUES ('OEUF',' tout ce qui contient des oeufs') ;  
INSERT INTO INTOLERANCE VALUES ('SURFITE',' sulfite') ;  

prompt ------------------------------------------;
prompt -----     insertion RESTAURANT    ------------;
prompt ------------------------------------------;

INSERT INTO RESTAURANT VALUES (0,'LA TABLE DE LA MER','36 avenue de la lune MONTPELLIER') ; 
INSERT INTO RESTAURANT VALUES (1,'FRITES ET MOULES','1 rue du mariage PARIS') ; 
INSERT INTO RESTAURANT VALUES (2,'TITOU PIZZA','690 rue de la vielle poste MONTPELLIER') ; 
INSERT INTO RESTAURANT VALUES (3,'RATA TA TOUILLE','1 champs elysees PARIS') ; 

prompt ------------------------------------------;
prompt -----     insertion PERSONNES  CLIENT ET INTOLERANT EMPLOYE   ------------;
prompt ------------------------------------------;

INSERT INTO PERSONNE VALUES (0,'BLAISE','GINETTE') ;  
INSERT INTO CLIENT VALUES (0,'blaise.gin@hotmail.fr') ;  
INSERT INTO INTOLERANT VALUES (0,'VEGAN') ; 
INSERT INTO INTOLERANT VALUES (0,'FRUIT DE MER') ; 
INSERT INTO INTOLERANT VALUES (0,'ARACHIDE') ; 

INSERT INTO PERSONNE VALUES (1,'BILAROYA','JEAN') ;  
INSERT INTO EMPLOYE VALUES (1,0,'PATRON') ; 

INSERT INTO PERSONNE VALUES (2,'MEYERE','RACHID') ;  
INSERT INTO CLIENT VALUES (2,'rachidou@hotmail.fr') ;  
INSERT INTO INTOLERANT VALUES (2,'ARACHIDE') ; 
INSERT INTO INTOLERANT VALUES (2,'OEUF') ; 

INSERT INTO PERSONNE VALUES (3,'GRAINDURI','FLORIAN') ;  
INSERT INTO CLIENT VALUES (3,'grainflo@hotmail.fr') ;  
INSERT INTO INTOLERANT VALUES (3,'ARACHIDE') ; 
INSERT INTO INTOLERANT VALUES (3,'LACTOSE') ; 

INSERT INTO PERSONNE VALUES (4,'GRADUBID','ARTHUR') ;  
INSERT INTO CLIENT VALUES (4,'gradu@hotmail.fr') ;  
INSERT INTO INTOLERANT VALUES (4,'TYRAMINE') ; 
INSERT INTO INTOLERANT VALUES (4,'SURFITE') ; 

INSERT INTO PERSONNE VALUES (5,'SURO','LUCIO') ;  
INSERT INTO CLIENT VALUES (5,'suro.lulu@hotmail.fr') ;  
INSERT INTO INTOLERANT VALUES (5,'DIABETE') ; 
INSERT INTO INTOLERANT VALUES (5,'GLUTEN') ; 

INSERT INTO PERSONNE VALUES (6,'HUGO','BRENO') ;  

INSERT INTO PERSONNE VALUES (7,'DEBOVARI','BERENICE') ;  
INSERT INTO EMPLOYE VALUES (7,1,'PATRON') ; 

INSERT INTO PERSONNE VALUES (8,'NANI','MARK') ;  
INSERT INTO EMPLOYE VALUES (8,2,'PATRON') ; 

INSERT INTO PERSONNE VALUES (9,'OBVIOUS','SIDI') ;  
INSERT INTO EMPLOYE VALUES (9,3,'PATRON') ; 

INSERT INTO PERSONNE VALUES (10,'CORONA','VICTOR') ;  
INSERT INTO EMPLOYE VALUES (10,2,'SERVEUR') ; 

INSERT INTO PERSONNE VALUES (11,'OCOCHA','DIDIER') ;  
INSERT INTO EMPLOYE VALUES (11,2,'CUISINIER') ; 

prompt ------------------------------------------;
prompt -----     insertion TABLE    ------------;
prompt ------------------------------------------;

INSERT INTO TABLES VALUES (0,0,2) ; 
INSERT INTO TABLES VALUES (1,0,3) ; 
INSERT INTO TABLES VALUES (2,1,4) ; 
INSERT INTO TABLES VALUES (3,2,4) ; 
INSERT INTO TABLES VALUES (4,2,4) ; 
INSERT INTO TABLES VALUES (5,2,2) ; 
INSERT INTO TABLES VALUES (6,3,3) ; 
INSERT INTO TABLES VALUES (7,3,3) ; 
INSERT INTO TABLES VALUES (8,3,5) ; 
INSERT INTO TABLES VALUES (9,3,3) ; 

prompt ------------------------------------------;
prompt -----     insertion RESERVATION    ------------;
prompt ------------------------------------------;

INSERT INTO RESERVATION VALUES (0,0,'30-11-2021 10:00:00','30-11-2021 12:00:00',2) ; 
INSERT INTO RESERVATION VALUES (1,2,'30-01-2021 22:00:00',' 31-01-2021 02:00:00',2) ; 
INSERT INTO RESERVATION VALUES (2,4,'01-12-2022 21:00:00','01-12-2022 23:00:00',2) ; 
INSERT INTO RESERVATION VALUES (3,5,'30-12-2019 08:00:00','30-12-2019 11:00:00',4) ; 

prompt ------------------------------------------;
prompt -----     insertion AVIS    ------------;
prompt ------------------------------------------;

INSERT INTO AVIS VALUES (0,0,2,'cetait pas ouf, le plat etait froid') ; 
INSERT INTO AVIS VALUES (1,0,2,'trop sale') ; 
INSERT INTO AVIS VALUES (2,0,4,'vraiment tres bon') ; 
INSERT INTO AVIS VALUES (3,0,0,'jai fais un intoxication alimentaire') ; 
INSERT INTO AVIS VALUES (3,5,4,'excelent et pas cher') ; 

prompt ------------------------------------------;
prompt -----     AFFICHAGE DE TOUT    ------------;
prompt ------------------------------------------;
prompt --                                      --;
prompt --                                      --;

prompt --|--------- CLIENTS ---------|------------------------------------------;;
SELECT * FROM CLIENT;

prompt --|--------- EMPLOYE ---------|------------------------------------------;;
SELECT * FROM EMPLOYE;

prompt --|--------- RESTAURANT ---------|------------------------------------------;;
SELECT * FROM RESTAURANT;

prompt --|--------- TABLES ---------|------------------------------------------;;
SELECT * FROM TABLES;

prompt --|--------- RESERVATION ---------|------------------------------------------;;
SELECT * FROM RESERVATION;

prompt --|--------- AVIS ---------|------------------------------------------;;
SELECT * FROM AVIS;

prompt --|--------- INTOLERANCE ---------|------------------------------------------;;
SELECT * FROM INTOLERANCE;

prompt ------------------------------------------;
prompt -----     REQUETES DE TEST    ------------;
prompt ------------------------------------------;
prompt --                                      --;
prompt --                                      --;

prompt -----     NOM, PRENOM, ET ROLE DANS LE RESTO, DES EMPLOYES   ------------;

SELECT PERSONNE.NOM,PRENOM,RESTAURANT.NOM AS RESTAURANT,TYPE 
FROM EMPLOYE,PERSONNE,RESTAURANT 
WHERE EMPLOYE.ID_EMPLOYE=PERSONNE.ID_PERSONNE AND EMPLOYE.ID_RESTO=RESTAURANT.ID_RESTO;

prompt -----     NOM, PRENOM, ET INTOLERANCES DES PERSONNES   ------------;

SELECT NOM,PERSONNE.PRENOM,INTOLERANT.ID_INTO AS INTOLERANCE
FROM PERSONNE,INTOLERANT
WHERE INTOLERANT.ID_CLIENT=PERSONNE.ID_PERSONNE;

prompt -----     INTOLERANCE TOUCHANT LE PLUS DE PERSONNES   ------------;

SELECT ID_INTO AS INTOLERANCE_LA_PLUS_COURANTE
FROM INTOLERANT 
GROUP BY ID_INTO
HAVING COUNT(ID_CLIENT) >= (
	SELECT MAX(COUNT(ID_CLIENT))
	FROM INTOLERANT 
	GROUP BY ID_INTO)
;


prompt -----     INTOLERANCE NE CONCERNANT PERSONNE   ------------;

SELECT INTOLERANCE.ID_INTO AS INTOLERANCE_NE_TOUCHANT_PERSONNE
FROM INTOLERANCE
WHERE INTOLERANCE.ID_INTO NOT IN (
	SELECT INTOLERANT.ID_INTO
	FROM INTOLERANT )
;

prompt -----     LES CLIENTS QUI ONT LAISSE UN AVIS A TOUT LES RESTAURANTS   ------------;

SELECT PERSONNE.NOM,PERSONNE.PRENOM
FROM CLIENT,PERSONNE
WHERE 	CLIENT.ID_CLIENT=PERSONNE.ID_PERSONNE
	AND NOT EXISTS (
		SELECT *
		FROM RESTAURANT
		WHERE NOT EXISTS (
			SELECT * 
			FROM AVIS
			WHERE AVIS.ID_CLIENT=CLIENT.ID_CLIENT AND AVIS.ID_RESTO=RESTAURANT.ID_RESTO
		)
	)
;


prompt -----     LES RESTAURANTS QUI ONT RECU DES RESERVATIONS CONCERNANT DES CLIENTS ALLERGIQUES A L'ARACHIDE   ------------;

SELECT DISTINCT RESTAURANT.NOM
FROM RESERVATION,RESTAURANT,TABLES
WHERE 	RESERVATION.ID_TABLE=TABLES.ID_TABLE
	AND TABLES.ID_RESTO=RESTAURANT.ID_RESTO
	AND EXISTS (
		SELECT *
		FROM INTOLERANT
		WHERE 	INTOLERANT.ID_INTO='ARACHIDE' 
			AND INTOLERANT.ID_CLIENT=RESERVATION.ID_CLIENT
	)
;






















